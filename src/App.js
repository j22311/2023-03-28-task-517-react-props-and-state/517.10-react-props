import logo from './logo.svg';
import './App.css';
import Info from './components/Info';

function App() {
  return (
    <div >
      <Info firstName="Hoang" lastName="An" favNumber={33} />
    </div>
  );
}

export default App;
