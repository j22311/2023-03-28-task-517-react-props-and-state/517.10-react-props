import { Component } from "react";

class Info extends Component {
    
    render() {
        let {firstName, lastName, favNumber} = this.props;
        console.log(this.props);
        return (
            <p>my name is {firstName} {lastName} and my favorite number is {favNumber}</p>
        )
    }
}

export default Info;